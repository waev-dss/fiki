import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";

export default function TitleCard({ title, text }) {
  return (
    <Card sx={{ padding: 4, margin: 2 }}>
      <Stack spacing={1}>
        <Typography
          sx={{ fontSize: 24, textAlign: "center" }}
          component={"span"}
          color="text.secondary"
          gutterBottom
        >
          {title}
        </Typography>
        <Typography
          sx={{ textAlign: "center" }}
          component={"span"}
          variant="body2"
        >
          {text}
        </Typography>
      </Stack>
    </Card>
  );
}
