import React from "react";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Timeline from "@mui/lab/Timeline";
import Typography from "@mui/material/Typography";
import CustomizedTimelineItem from "./CustomizedTimelineItem";
import TimelineCard from "./TimelineCard";

export default function CustomizedTimeline(props) {
  const generateTimelineItems = () => {
    if (props.items === undefined || props.items.length === 0) {
      return <CustomizedTimelineItem label="TBD" />;
    }

    return props.items.map((item, idx) => {
      return (
        <CustomizedTimelineItem
          key={idx}
          color={item.color}
          icon={item.icon}
          label={item.label}
        >
          <TimelineCard
            title={item.title}
            content={item.content}
            button={
              <Link style={{ textDecoration: "none" }} to={`milestone/${idx}`}>
                <Button variant="outlined">{item.buttonText}</Button>
              </Link>
            }
          />
        </CustomizedTimelineItem>
      );
    });
  };

  return (
    <Box
      sx={{ display: "flex", flexDirection: "column", alignContent: "center" }}
    >
      <Typography textAlign="center" variant="h2" gutterBottom component="div">
        Timeline
      </Typography>
      <Timeline position="alternate" sx={{ margin: 0 }}>
        {generateTimelineItems()}
      </Timeline>
    </Box>
  );
}
