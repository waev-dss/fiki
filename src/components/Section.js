import Box from "@mui/material/Box";

export default function Section(props) {
  return (
    <Box
      sx={{
        padding: 5,
        minHeight: "75vh",
        width: "100vw",
        backgroundColor: `${props.backgroundColor}`,
      }}
    >
      {props.children}
    </Box>
  );
}
