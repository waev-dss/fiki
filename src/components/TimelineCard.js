import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function TimelineCard(props) {
  return (
    <Card
      sx={{
        minWidth: 275,
        transition: "box-shadow .3s",
        "&:hover": {
          boxShadow: 10,
        },
      }}
    >
      <CardContent>
        <Typography sx={{ fontSize: 24 }} color="text.secondary" gutterBottom>
          {props.title}
        </Typography>
        <Typography variant="body2" style={{ whiteSpace: "pre-line" }}>{props.content}</Typography>
      </CardContent>
      <CardActions sx={{ padding: 1 }}>{props.button}</CardActions>
    </Card>
  );
}
