import { Switch, Route, useRouteMatch } from "react-router-dom";
import MilestonePage from "./MilestonePage";

export default function MilestoneRouter() {
  let match = useRouteMatch();

  return (
    <div>
      <Switch>
        <Route path={`${match.path}/:milestoneId`}>
          <MilestonePage />
        </Route>
      </Switch>
    </div>
  );
}
