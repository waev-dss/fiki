import Chart from "react-google-charts";

export default function DonutChart({ data, title }) {
  return (
    <Chart
      width={"100%"}
      height={"400px"}
      chartType="PieChart"
      loader={<div>Loading Chart</div>}
      data={data}
      options={{
        pieHole: 0.4,
      }}
    />
  );
}
