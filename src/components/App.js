import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import CustomizedTimeline from "./CustomizedTimeline";
import Section from "./Section";
import MilestoneRouter from "./MilestoneRouter";

import timelineData from "../constants/timeline";
import NavBar from "./NavBar";
import TeamMembers from "./TeamMembers";

import { createTheme, ThemeProvider } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      light: "#03a9f4",
      main: "#0288d1",
      dark: "#42a5f5",
    },
    secondary: {
      light: "#03a9f4",
      main: "#0288d1",
      dark: "#01579b",
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router basename="/fiki">
        <div>
          <NavBar />
          <Switch>
            <Route path="/team">
              <TeamMembers />
            </Route>
            <Route path="/milestone">
              <Section backgroundColor="primary.light">
                <MilestoneRouter />
              </Section>
            </Route>
            <Route path="/">
              <Section backgroundColor="secondary.light">
                <CustomizedTimeline items={timelineData} />
              </Section>
              <Section>
                <TeamMembers />
              </Section>
            </Route>
          </Switch>
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
