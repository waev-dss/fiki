import React, { Fragment } from "react";
import Stack from "@mui/material/Stack";

export default function CustomizedStack({ children }) {
  const generateStackItems = () => {
    if (children === undefined) {
      return <div>Nothing in stack...</div>;
    }

    return React.Children.map(children, (child, idx) => {
      return <Fragment key={idx}>{child}</Fragment>;
    });
  };

  return <Stack spacing={1}>{generateStackItems()}</Stack>;
}
