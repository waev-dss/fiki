import * as React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TeamMember from "./TeamMember";

const members = [
  {
    name: "Robert Aron",
    image: `${process.env.PUBLIC_URL}/team/robert_aron.jpg`,
    role: "Backend developer",
  },
  {
    name: "Dorin Geman",
    image: `${process.env.PUBLIC_URL}/team/dorin_geman.jpg`,
    role: "Product Owner",
  },
  {
    name: "Laurențiu Olteanu",
    image: `${process.env.PUBLIC_URL}/team/laurentiu_olteanu.jpg`,
    role: "Frontend developer",
  },
  {
    name: "Andrei Serițan",
    image: `${process.env.PUBLIC_URL}/team/andrei_seritan.jpg`,
    role: "Frontend developer",
  },
  {
    name: "Andreea Strejovici",
    image: `${process.env.PUBLIC_URL}/team/andreea_strejovici.jpg`,
    role: "Test Lead",
  },
];

export default function TeamMembers() {
  return (
    <Box
      sx={{
        padding: 2,
        display: "flex",
        alignContent: "center",
        flexDirection: "column",
      }}
    >
      <Typography textAlign="center" variant="h2" gutterBottom component="div">
        Team
      </Typography>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-evenly",
          alignContent: "center",
        }}
      >
        {members.map((member, idx) => (
          <TeamMember
            key={idx}
            name={member.name}
            image={member.image}
            role={member.role}
          ></TeamMember>
        ))}
      </Box>
    </Box>
  );
}
