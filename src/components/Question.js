import { Typography } from "@mui/material";
import { Box } from "@mui/system";

export default function Question({ question, answer, align }) {
  return (
    <Box textAlign={align}>
      <Typography variant="body1" component="h2" fontWeight="bold">
        {question}
      </Typography>
      <Typography variant="body2" component="h2">
        {answer}
      </Typography>
    </Box>
  );
}
