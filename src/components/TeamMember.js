import * as React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";

export default function TeamMember({ name, image, role }) {
  return (
    <Card
      sx={{
        padding: 2,
        textAlign: "center",
        transition: "box-shadow .3s",
        "&:hover": {
          boxShadow: 10,
        },
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignContent: "center",
          justifyContent: "center",
          maxWidth: "150px",
          maxHeight: "150px",
          marginBottom: "15px",
        }}
      >
        <Avatar sx={{ width: "100%", height: "auto" }} alt={name} src={image} />
      </Box>
      <Typography variant="body1" component="h2" fontWeight="bold">
        {name}
      </Typography>
      <Typography variant="body2" component="h2">
        {role}
      </Typography>
    </Card>
  );
}
