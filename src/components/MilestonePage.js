import { useParams } from "react-router-dom";

import CustomizedStack from "./CustomizedStack";
import CustomizedStackItem from "./CustomizedStackItem";
import milestonesDescription from "../constants/milestones";

export default function MilestonePage() {
  let { milestoneId } = useParams();

  const generateStackItems = (milestone) => {
    if (milestone < 0 || milestone >= milestonesDescription.length) {
      return <div>Milestone does not exist</div>;
    }

    return milestonesDescription[milestone].sections.map((section, idx) => {
      return (
        <CustomizedStackItem
          key={idx}
          leftItem={section.leftItem}
          leftSize={section.leftSize}
          rightItem={section.rightItem}
          rightSize={section.rightSize}
          reverse={idx % 2 !== 0}
        />
      );
    });
  };

  return <CustomizedStack>{generateStackItems(milestoneId)}</CustomizedStack>;
}
