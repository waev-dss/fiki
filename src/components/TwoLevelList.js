import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

export default function TwoLevelList({ objects }) {
  return (
    <Box>
      {objects.map((object, idx) => {
        return (
          <Box key={idx}>
            <Typography variant="body1" component="h2" fontWeight="bold">
              {object.category}
            </Typography>
            {object.category === undefined ? (
              <Typography variant="body1" component="h2">
                {object}
              </Typography>
            ) : (
              <React.Fragment />
            )}
            {object.items?.map((item, idx) => {
              return (
                <Typography key={idx} variant="body2" component="h2">
                  {item}
                </Typography>
              );
            })}
          </Box>
        );
      })}
    </Box>
  );
}