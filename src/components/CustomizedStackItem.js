import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";

export default function CustomizedStackItem({
  leftItem,
  rightItem,
  leftSize,
  rightSize,
  reverse,
}) {
  const generateGridItem = (item, size) => {
    if (item === undefined) {
      return;
    }
    return (
      <Grid item xs={size}>
        {item}
      </Grid>
    );
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container justifyContent="center" alignItems="center" spacing={2}>
        {reverse
          ? generateGridItem(rightItem, rightSize)
          : generateGridItem(leftItem, leftSize)}
        {reverse
          ? generateGridItem(leftItem, leftSize)
          : generateGridItem(rightItem, rightSize)}
      </Grid>
    </Box>
  );
}
