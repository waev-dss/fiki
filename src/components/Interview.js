import { Box } from "@mui/system";
import Question from "./Question";

export default function Interview({ questions, answers, align }) {
  const paired = questions.map((question, idx) => {
    return [question, answers[idx]];
  });

  return (
    <Box textAlign={align}>
      {paired.map((pair, idx) => {
        return <Question key={idx} question={pair[0]} answer={pair[1]} />;
      })}
    </Box>
  );
}
