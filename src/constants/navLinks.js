import timelineData from './timeline';

const links = timelineData.map(({title}, index) => {
    return {
        name: title,
        link: `/milestone/${index}`
    }});

export default links;
