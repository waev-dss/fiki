import DescriptionRoundedIcon from "@mui/icons-material/DescriptionRounded";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { Category, FilterFrames, HowToReg, MonetizationOn, Slideshow, Storefront, SupervisedUserCircle } from "@mui/icons-material";

const timelineData = [
  {
    icon: <DescriptionRoundedIcon />,
    color: "primary",
    label: "M1",
    title: "Project description",
    content: "Meet Waev - Why, What, Who, How",
    buttonText: "Read more",
  },
  {
    icon: <CheckCircleOutlineIcon />,
    color: "secondary",
    label: "M2",
    title: "Early Validation",
    content:`Curious about how we identified the problem?
      Do you want to find out how we tested the problem and the solution?
      Are we going to change something after this validation?`,
    buttonText: 'Read more'
  },
  {
    icon: <FilterFrames />,
    color: 'secondary',
    label: 'M3',
    title: 'Wireframe and Landing Page',
    content: `Intrigued by how Waev would look and feel?
      Want to have early access to matching?`,
    buttonText: 'Read more'
  },
  {
    icon: <SupervisedUserCircle />,
    color: 'secondary',
    label: 'M4',
    title: 'User Experience',
    content: `Who would use Waev?
      What do users want from Waev?`,
    buttonText: 'Read more'
  },
  {
    icon: <HowToReg />,
    color: 'secondary',
    label: 'M5',
    title: 'Lead Generation and Collection',
    content: `How are people interested in Waev?
      How can you help?`,
    buttonText: 'Read more'
  },
  {
    icon: <Storefront />,
    color: 'secondary',
    label: 'M6',
    title: 'Market Share',
    content: `Who is our target market?
      Who are we competing with?
      How much can we gain over them?`,
    buttonText: 'Read more',
  },
  {
    icon: <Category />,
    color: 'secondary',
    label: 'M7',
    title: 'MVP',
    content: `What is our MVP looking like?
      What technologies were used?`,
    buttonText: 'Read more',
  },
  {
    icon: <MonetizationOn />,
    color: 'secondary',
    label: 'M8',
    title: 'First sale',
    content: `Who are the first users?
      What do they think about the product?`,
    buttonText: 'Read more',
  },
  {
    icon: <Slideshow />,
    color: 'secondary',
    label: 'M9',
    title: 'Final presentation',
    content: `Find out what our final presentation is!`,
    buttonText: 'Read more',
  },
];

export default timelineData;
