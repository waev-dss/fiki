import Box from "@mui/material/Box";
import { Card, Stack, Grid } from "@mui/material";
import TitleCard from "./../components/TitleCard";
import DonutChart from "../components/DonutChart";
import Interview from "../components/Interview";
import TwoLevelList from "../components/TwoLevelList";

const interview = {
  questions: [
    "Out of the existing dating apps, which one piqued your interest until now and why?",
    "What are you looking for in a dating app?",
    "If you could choose what to see on a person's profile, what would intrigue you the most?",
    "How important is a person's appearance for you? How about voice?",
    "When thinking about a future date, what can be a dealbreaker for you?",
    "How would you characterise this person after listening to these recordings?",
    "Now that you have tried it, how do you feel about meeting someone through their voice?"
  ],
  people: [
    [
      "Until now I used just Tinder and in the beginning I tried because I was curious, but after meeting some nice people I started to enjoy it a little bit more.",
      "To find someone that has a lot of things in common with me.",
      "Their looks and hobbies.",
      "For me both their appearance and voice are quite important.",
      "Not having the same interests and same kind of humour",
      "I think that the person from the recording is a nice person, but a little bit weird.",
      "I think it is nice to hear their voices, but I think it would help if you could see them too.",
    ],
    [
      "I haven’t used any dating apps so far.",
      "Hm, I would like to use an app to easily find people to hang out with.",
      "The passions. The favourite goto places during a holiday. A bucket list. :)",
      "The appearance is clearly the first thing upon which you might get some information about that person and it also has to be pleasant based on your preferences. The voice, hm, I haven’t thought about that before - I tend to say that it can be more intimate.",
      "The things that we have in common. The things that I can learn about from the other person.",
      "It’s fine, but I would definitely like to also see some photos ;P.",
      "Only based on their voice? Pretty weird to be honest, I wouldn't do that.",
    ],
    [
      "I can personally say that I've only tried two dating apps: Tinder and Bumble. Both have differentiating features that can make my experience better or worse depending on the situation.",
      "For me, it would probably be the ability of learning something about that person. As of now, I have noticed that the appearance of the people that are on these apps is literally all that matters… It's not the same for everyone, and I would like to know that somebody wants to meet me for who I am, not how I look.",
      "Pictures and getting to know someone through descriptions or random fun facts would probably still be a good choice, but I think that it's also about the way the information is delivered to you. Having a profile publicly available from the beginning would probably give an unfair advantage to some people over the others.",
      "Appearance is important… But, at the same time, I would rather meet someone I have common interests and know I can have a fun time with. Voice is an interesting topic for me. I think I can get to a point where I'd put voice slightly above looks.",
      "Because you've brought it up in the last question, the voice can be a dealbreaker for me. Also probably on that list would be having no common interests or not loving animals.",
      "Hmm, I would say that she's probably quite young, with an adventurous mindset. From what I've heard, it seems that we might have some common music interests as well. Definitely caught my interest.",
      "Sounds like a great idea! Unintended pun.",
    ],
  ]
}

const persona = [
  {
    category: "Demographics",
    items: ["Early 20s.", "Socially active."]
  },
  {
    category: "Behaviour",
    items: ["Feels alone and wants to try out new things.", "Enjoys sharing activities with others."]
  },
  {
    category: "Needs",
    items: ["A close person who shares the same hobbies.", "Ideas for hobbies and activities."]
  },
  {
    category: "Problems",
    items: ["I need context to meet someone.", "Low confidence level when talking about looks."]
  }
]

const userStories = [
  "I want to meet someone new.",
  "I want to try out some new experiences.",
  "I want somebody to look after me despite my looks.",
  "I want to know what other people are doing.",
  "I want to be able to find buddies wherever I travel.",
]

const milestonesDescription = [
  {
    name: "Milestone 1",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="Problem"
            text="Most online dating applications on the market are based on the same ideas reused for over ten years. The users feel this stagnation regarding the lack of new ways of interaction with people. Also, the general opinion about online dating tends to become negative. The main argument behind this is that applications such as Tinder or Bumble promote superficiality and create an environment where there can be discrimination."
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
              }}
              src={`${process.env.PUBLIC_URL}/milestone_1/problem.png`}
              alt="problem"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Solution"
            text="Waev is a dating app where the first interaction is the voice. While you swipe through profiles, you’ll be able to see the person’s name and hear some voice recordings. As you start chatting with a match, you can unlock progressively more of your profiles, including some photos."
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
              }}
              src={`${process.env.PUBLIC_URL}/milestone_1/solution.png`}
              alt="solution"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Customer segments"
            text="We target anyone with a smartphone that wants to form a meaningful connection based on the advantages of the voice interaction (desire, belief, visual impairment)."
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_1/customer_segments.png`}
              alt="customer segments"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Competitors"
            text={
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Stack spacing={3}>
                  <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                  >
                    <Grid item xs={6}>
                      <Card
                        sx={{
                          padding: 2,
                          transition: "box-shadow .3s",
                          "&:hover": {
                            boxShadow: 10,
                          },
                        }}
                      >
                        <img
                          style={{ maxHeight: "100%", maxWidth: "100%" }}
                          src={`${process.env.PUBLIC_URL}/competitors/tinder.png`}
                          alt="tinder-logo"
                        ></img>
                      </Card>
                    </Grid>
                    <Grid item xs={6}>
                      <Card
                        sx={{
                          padding: 2,
                          transition: "box-shadow .3s",
                          "&:hover": {
                            boxShadow: 10,
                          },
                        }}
                      >
                        <img
                          style={{ maxHeight: "100%", maxWidth: "100%" }}
                          src={`${process.env.PUBLIC_URL}/competitors/waving.png`}
                          alt="waving-logo"
                        ></img>
                      </Card>
                    </Grid>
                  </Grid>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    <img
                      style={{ maxHeight: "100%", maxWidth: "100%" }}
                      src={`${process.env.PUBLIC_URL}/competitors/bumble.png`}
                      alt="bumble-logo"
                    ></img>
                  </Card>
                </Stack>
              </Box>
            }
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="Advantages over the competition"
            text={
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Stack direction="row" spacing={3}>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Ease of use for users with visual impairment - accessibility
                  </Card>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Explores a new concept in the field of online dating - voice
                    instead of photos
                  </Card>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Discourage the superficiality and creates an environment
                    without discrimination
                  </Card>
                </Stack>
              </Box>
            }
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="Key metrics"
            text={
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Stack direction="row" spacing={3}>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    User engagement
                  </Card>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Number of premium subscriptions
                  </Card>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Relevancy of new features
                  </Card>
                </Stack>
              </Box>
            }
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="Cost structure"
            text={
              <DonutChart
                data={[
                  ["Cost", "% of total budget"],
                  ["Marketing", 50],
                  ["Cloud services", 20],
                  ["Development", 20],
                  ["Consulting", 10],
                ]}
              />
            }
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                padding: 60,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_1/cost_structure.png`}
              alt="cost_structure"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Revenue streams"
            text={
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Stack direction="row" spacing={3}>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Freemium plan
                  </Card>
                  <Card
                    sx={{
                      padding: 2,
                      transition: "box-shadow .3s",
                      "&:hover": {
                        boxShadow: 10,
                      },
                    }}
                  >
                    Partnerships with companies interested in voice recognition
                  </Card>
                </Stack>
              </Box>
            }
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
              }}
              src={`${process.env.PUBLIC_URL}/milestone_1/revenue_streams.png`}
              alt="revenue_streams"
            />
          </Box>
        ),
        rightSize: 4,
      },
    ],
  },
  {
    name: "Milestone 2",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="How we identified the problem"
            text="We know many people that were disappointed because they discovered that the appearances of other people do not match their personality when they get to meet them."
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
              }}
              src={`${process.env.PUBLIC_URL}/milestone_2/identify-problem.png`}
              alt="problem"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="What we thought is a solution for the problem"
            text={
              <p>
                Based on certain studies we chose voice as a better trait than
                appearance when talking about discovering a person’s personality
                and establishing a meaningful connection with them. Although it
                is easy to decipher and understand the words that someone is
                transmitting through their message, the voice has many other
                parameters that are yet to be fully interpreted - such as
                resonance, pitch, volume, respiratory dynamics and vocal
                registers. As some projects have observed and concluded, these
                voice parameters can easily offer the personality traits,
                demographical traits and physical traits of the person speaking
                <a
                  href="https://doi.org/10.1007/BF01074441"
                  target="_blank"
                  rel="noreferrer"
                >
                  [1]
                </a>
                <a
                  href="https://doi.org/10.1145/3386392.3397592"
                  target="_blank"
                  rel="noreferrer"
                >
                  [2]
                </a>
                <a
                  href="https://doi.org/10.1145/3123266.3123338"
                  target="_blank"
                  rel="noreferrer"
                >
                  [3]
                </a>
                . One of the first studies that connects the human voice with
                sociological traits dates back to 1958
                <a
                  href="https://doi.org/10.1080/03637755809375240"
                  target="_blank"
                  rel="noreferrer"
                >
                  [4]
                </a>
                , so the natural question that was raised was what else can we
                link to our voice? In what way how we speak and what we say can
                be interpreted in figuring out personal and maybe hidden
                characteristics about ourselves? The voice is the new pick-up
                trend.
              </p>
            }
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
              }}
              src={`${process.env.PUBLIC_URL}/milestone_2/solution.png`}
              alt="solution"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Customer discovery plan"
            text="We have decided to conduct an online survey to determine whether or not people are willing to try out a different app concept. "
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_2/customer-discovery.png`}
              alt="customer segments"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Describe the process in detail"
            text={[...Array(24).keys()].map((idx) => {
              return (
                <Box sx={{ padding: 2 }}>
                  <img
                    src={`${process.env.PUBLIC_URL}/milestone_2/${idx + 1}.png`}
                    alt={`${idx} is loading...`}
                  />
                </Box>
              );
            })}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard title="How these insights have affected our product?"
          text={
            <div>
              <p>
                Our survey has concluded that the current online dating applications have a very low conversion rate from a match to an actual face to face date. Our conclusion is that when people scroll through other profiles, they’re making snap decisions based on what they’re seeing, most of the time not giving too much thought to it.
              </p>
              <p>
                By taking into consideration what traits are the most successful in getting a relationship, we concluded that physical appearance is just as important as voice characteristics or habits. This means that we can effectively use voice as the main highlight of the application as well as using photos at a later stage to establish a deeper connection.
              </p>
              <p>
                The form included a demo of the voice matching experience which received positive feedback from people. 21 out of 27 responders rated the experience above 7, a trend that we could see male and female participants alike.
              </p>
            </div>
          } />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard title="Are we ready for the next stage?" text="We have our features stated above so we are ready to continue with the design of the app and how we will have them integrated." />
        ),
        leftSize: 8,
      },
    ],
  },
  {
    name: "Milestone 3",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="Wireframe"
            text={<Box sx={{ padding: 2 }}>
                The wireframe can be viewed interactively <a href="https://www.figma.com/file/1TaEKmHmMMbEBZxXWREYMn/Design-front-end----Final?node-id=0%3A1">here</a>!
            </Box>}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_3/figma.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Pictures"
            text={[...Array(10).keys()].map((idx) => {
              return (
                <Box sx={{ padding: 2 }}>
                  <img
                    src={`${process.env.PUBLIC_URL}/milestone_3/${idx + 1}.png`}
                    alt={`${idx} is loading...`}
                  />
                </Box>
              );
            })}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="Landing page"
            text={<Box sx={{ padding: 2 }}>
                <a href="https://waev-team.gitlab.io/landing-page">This</a> is Waev's landing page!
            </Box>}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_3/landing-page.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
    ]
  },
  {
    name: "Milestone 4",
    sections: 
      interview.people.map((answers, idx) => {
        
        var obj = {
          leftItem: (
            <TitleCard
              title={"Interview " + (idx + 1)}
              text={<Box sx={{ padding: 2 }}>
                    <Interview maxWidth="100px" questions={interview.questions} answers={answers} align={idx % 2 === 1 ? "left" : "right"}/>
              </Box>}
            />
          ),
          leftSize: 8,
          rightItem: (
            <Box sx={{ display: "flex", justifyContent: "center" }}>
              <img
                style={{
                  objectFit: "contain",
                  maxWidth: "100%",
                  maxHeight: "100%",
                  marginBottom: 40,
                  marginTop: 40,
                }}
                src={`${process.env.PUBLIC_URL}/milestone_4/interview.png`}
                alt="figma"
              />
            </Box>
          ),
          rightSize: 4
        }
        return obj
      })
    .concat(
    [
      {
        leftItem: (
          <TitleCard
            title="Personas"
            text={
                <Box sx={{ padding: 2 }}>
                  From the three interviews we've managed to identify the following user persona.
                  <TwoLevelList objects={persona}/>
                </Box>
            }
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_4/persona.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="User case"
            text={<Box sx={{ padding: 2 }}>
                We made the following user case diagram.
                <Box sx={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{
                      objectFit: "contain",
                      maxWidth: "100%",
                      maxHeight: "100%",
                      marginBottom: 40,
                      marginTop: 40,
                    }}
                    src={`${process.env.PUBLIC_URL}/milestone_4/user_case.png`}
                    alt="figma"
                  />
                </Box>
            </Box>}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="User stories"
            text={<TwoLevelList objects={userStories} />}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_4/user_story.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Wireframe updates"
            text={<Box sx={{ padding: 2 }}>
                Changed color of dislike, play/pause button to improve user experience.
                <Box sx={{ display: "flex", justifyContent: "center" }}>
                  <img
                    style={{
                      objectFit: "contain",
                      maxWidth: "100%",
                      maxHeight: "100%",
                      marginBottom: 40,
                      marginTop: 40,
                    }}
                    src={`${process.env.PUBLIC_URL}/milestone_4/ui_update.png`}
                    alt="figma"
                  />
                </Box>
            </Box>}
          />
        ),
        leftSize: 8,
      },
    ])
  },
  {
    name: "Milestone 4",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="Lead collection results"
            text={<Box sx={{ padding: 2 }}>
                We have managed to gather 8 email subscriptions from potential users that have a strong desire to try out product.
            </Box>}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/email_list.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Sharing on Whatsapp"
            text={<Box sx={{ padding: 2 }}>
                To target people in their early 20's that are socially active we chose a Pokemon Go Whatsapp group.
            </Box>}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/whatsapp.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Sharing on Facebook Messenger"
            text={<Box sx={{ padding: 2 }}>
                In the team we don't use Messenger groups often, and we are not part of any significant big ones. However, we have a diverse set of friends so we messaged ideal ones privately to tell about our landing page.
            </Box>}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/facebook.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Sharing on Discord"
            text={<Box sx={{ padding: 2 }}>
                eFapp (Extreme Fappers) is a Counter-Strike: Global Offensive community with a Discord server. This community has mostly people in their 20's that are socially active and play a lot of video games.
            </Box>}
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/discord.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 7,
      },
      {
        leftItem: (
          <TitleCard
            title="Hotjar desktop mouse movement"
            text={<Box sx={{ padding: 2 }}>
                Mouse heatmap taken from Hotjar reveals that people highlight words like 'Listen' or 'Discover'. Most of them cross their mouse to the 'Omelet Du Fromage' photo to acknowledge the familiarity.
                However, people spend more time reading, than taking the action.
            </Box>}
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/movement-desktop.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 5,
      },
      {
        leftItem: (
          <TitleCard
            title="Hotjar desktop clicks"
            text={<Box sx={{ padding: 2 }}>
                The number of desktop clicks is rather small and does not reveal a whole lot. Definite clicks on the type field and submit buttons can be recognized.
            </Box>}
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/click-desktop.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 5,
      },
      {
        leftItem: (
          <TitleCard
            title="Hotjar phone scroll"
            text={<Box sx={{ padding: 2 }}>
                Phone metrics are definetly more abundant than desktop ones. Even though 77.8% of people reached the action part of the landing-page, the focus seens to be lost after the first 3 paragraphs.
            </Box>}
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/scroll-phone.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 1.25,
      },
      {
        leftItem: (
          <TitleCard
            title="Hotjar phone taps"
            text={<Box sx={{ padding: 2 }}>
                The phone users seem to have a clearer understanding of what the sole purpose of the page is: to input and submit their email.
            </Box>}
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_5/click-phone.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 1.25,
      },
    ]
  },
  {
    name: "Milestone 5",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="Target Market"
            text={<Box sx={{ padding: 2 }}>
              We target the same audience as the traditional dating apps do.
              <p>The goal is to bring a twist to the people - using their voice as the first interaction.</p>
            </Box>}
          />
        ),
        leftSize: 10,
      },
      {
        leftItem: (
          <TitleCard
            title="Global Dating App Users"
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_6/users.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 5,
      },
      {
        leftItem: (
          <TitleCard
            title="Global Dating App Revenue"
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_6/revenue.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 5,
      },
      {
        leftItem: (
          <TitleCard
            title="Competitors"
            text={<Box sx={{ padding: 2 }}>
              <p>It may seem that the market for creating a dating app is overloaded, but more than 2000 businesses appeared in the dating service industry in 2019.</p>
              <p>Starting with Romania, we know from some sources that Tinder has a market share of over 70%. And Bumble is rising fast, being close to 55%.</p>
              <p>We are waiting for a <a href="https://www.statista.com" target="_blank" rel="noreferrer"> statista</a> subscription. Once we receive it we'll be able to get more insights.</p>
            </Box>}
          />
        ),
        leftSize: 5,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_6/competitors.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 5,
      },
      {
        leftItem: (
          <TitleCard
            title="Future Market Share"
            text={<Box sx={{ padding: 2 }}>
              <p>The opportunity of growth is underpinned by two aspects:
                <ul>
                  <li>The level of growth in the marketplace - that is, the growth phase of the product life-cycle, where new consumers start installing/purchasing the product category for the first time.</li>
                  <p>A BCG Matrix for this is under development as we're still missing some data.</p>
                  <li>The growth from consumers switching to the brand from competitors.</li>
                  <p>We focus on this option aswell as many users from what we've noticed so far are using multiple dating apps. Of course that the goal is sometimes different.
                    But still the average user doesn't use more than 3 apps for the same thing, so here's one place where we want to kick in in the beggining.
                  </p>
                </ul>
              </p>
              <p>We plan to reach 3% market share within the first year and bring it close to 10% by the end of the second year.</p>
              <p>Afterwards, we expect a growth factor between 1,3-5% every year.</p>
            </Box>}
          />
        ),
        leftSize: 10,
      },
      {
        leftItem: (
          <TitleCard
            title="Future Market Value"
            text={<Box sx={{ padding: 2 }}>
              <p>Considering our freemium business model and that 25-40% of users will pay 10$ for the subscription, at the end of the first 2 years we expect to gain a value of around $1,5M.</p>
              <p>By the end of the fifth year, with a global market that rises by 10% we expect a total value of $7M.</p>
            </Box>}
          />
        ),
        leftSize: 10,
      },
    ],
  },
  {
    name: "Milestone 6",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="Minimum Viable Product"
            text={<Box sx={{ padding: 2 }}>
                The MVP was build as a web application such that people can easily try it from their browser of choice.
            </Box>}
          />
        ),
        leftSize: 8,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "60%",
                maxHeight: "60%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/waev-logo.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Technologies used"
            text={
                <Box sx={{ padding: 2 }}>
                    Frontend was made with React.<br/>
                    Backend was made with NodeJS.<br/>
                    Database used was MySQL.<br/>
                </Box>
            }
          />
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/react-node-mysql.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Login and profile setup page"
            text={<Box sx={{ padding: 2 }}>
                Here users are able to create an account, login and complete the mandatory profile setup in order to view first profiles.
                The information given by the user is gender, at least one written interesting fact about them, at least one photo and
                a recording of their voice saying something about a topic.
            </Box>}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/2-profile-page.png`}
              alt="figma"
            />
          </Box>
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/1-login-page.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Discover page"
            text={<Box sx={{ padding: 2 }}>
                After account setup, users can listen to other people's recording on different topics and decide if they like them or not.
            </Box>}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/3-discover-page.png`}
              alt="figma"
            />
          </Box>
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/4-discover-page.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
      {
        leftItem: (
          <TitleCard
            title="Matching"
            text={<Box sx={{ padding: 2 }}>
                When other people like you back you can check out their profiles on the matching tab.
                On the matched profile, people can read facts about see photos of the person in question.
            </Box>}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/6-match-profile-page.png`}
              alt="figma"
            />
          </Box>
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_7/5-matches-page.png`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 4,
      },
    ]
  },
  {
    name: "Milestone 7",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="First sale"
            text={<Box sx={{ padding: 2 }}>
              We reached out to 3 people for feedback after sharing with them our product page and the web appplication.
            </Box>}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="Ionuț:"
            text={
              <Box sx={{ padding: 2 }}>
                Mi se pare puțin dubioasă ideea, dar în același timp interesantă.
                Am clasica problemă că mi se pare că vocea mea sună foarte ciudat, haha.
                Dar aș instala-o și aș folosi-o, nu neg, ar fi fun. Mai ales că probabil aș primi premium moca, nu - wink, wink -?
              </Box>
            }
          />
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_8/p1.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 2,
      },
      {
        leftItem: (
          <TitleCard
            title="Cristiana:"
            text={
              <Box sx={{ padding: 2 }}>
                Wow, ce avangardist :) Ar fi drăguț să-ți poți pune totuși ca un prim avatar ceva gen bitmoji de pe iOS.
                Nu-mi place că dacă nu am căști, nu prea o pot folosi.
                Dacă ești dispus să investești ceva mai mult timp și cauți ceva serios, pare o idee bună.
              </Box>
            }
          />
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_8/p2.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 2,
      },
      {
        leftItem: (
          <TitleCard
            title="Razvan:"
            text={
                <Box sx={{ padding: 2 }}>
                  Culorile din aplicatie sunt total pe langa, parerea mea. 
                  Iar ca idee, sincer, nu stiu cat poate prinde - trebuie sa investesti
                  mai mult timp decat pe una clasica si sincer, mie imi place sa vad poze
                  prin care sa dau swipe rapid. In plus, o sa-si mai poata promova instagramul fetele care arata bine??
                </Box>
            }
          />
        ),
        leftSize: 4,
        rightItem: (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              style={{
                objectFit: "contain",
                maxWidth: "100%",
                maxHeight: "100%",
                marginBottom: 40,
                marginTop: 40,
              }}
              src={`${process.env.PUBLIC_URL}/milestone_8/p3.jpg`}
              alt="figma"
            />
          </Box>
        ),
        rightSize: 2,
      },
      {
        leftItem: (
          <TitleCard
            title="Conclusion"
            text={<Box sx={{ padding: 2 }}>
              These are the main ideas drawn out.
              The people are a bit skeptical, but that’s obvious in the context of something different from the classical ideas.
            </Box>}
          />
        ),
        leftSize: 8,
      },
    ]
  },
  {
    name: "Milestone 8",
    sections: [
      {
        leftItem: (
          <TitleCard
            title="Final presentation"
            text={<Box sx={{ padding: 2 }}>
                This is our final presentation!
            </Box>}
          />
        ),
        leftSize: 8,
      },
      {
        leftItem: (
          <TitleCard
            title="Slides"
            text={[...Array(11).keys()].map((idx) => {
              return (
                <Box sx={{ padding: 2 }}>
                  <img
                    src={`${process.env.PUBLIC_URL}/milestone_9/presentation-${idx}.png`}
                    alt={`${idx} is loading...`}
                  />
                </Box>
              );
            })}
          />
        ),
        leftSize: 8,
      },
    ]
  }
];

export default milestonesDescription;
